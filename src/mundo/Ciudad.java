package mundo;

public class Ciudad {
	
	private String nombreCiudad;
	
	public Ciudad (String nombreCiudad){
		this.nombreCiudad = nombreCiudad;
	}
	
	public String darNombreCiudad ( ){
		return nombreCiudad;
	}

}
