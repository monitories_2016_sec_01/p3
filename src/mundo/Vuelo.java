package mundo;

import java.util.Date;

public class Vuelo {
	
	private String aerolinea;
	private int numeroVuelo;
	private Date horaSalida;
	private Date horaLlegada;
	private String tipoEquipo;
	private int numeroSillas;
	private boolean[] operacion;
	private Ciudad origen;
	private Ciudad destino;
	private boolean tipoVuelo;
	
	public Vuelo (String aerolinea, int numeroVuelo, Date horaSalida, Date horaLlegada, String tipoEquipo, int numeroSillas, boolean[] operacion, Ciudad origen, Ciudad destino, boolean tipoVuelo ){
		this.aerolinea = aerolinea;
		this.numeroVuelo = numeroVuelo;
		this.horaSalida = horaSalida;
		this.horaLlegada = horaLlegada;
		this.tipoEquipo = tipoEquipo;
		this.numeroSillas = numeroSillas;
		this.operacion = operacion;
		this.origen = origen;
		this.destino = destino;
		this.tipoVuelo = tipoVuelo;
	}
	
	public String darAerolinea ( ) {
		return aerolinea;
	}
	
	public int darNumeroVuelo ( ) { 
		return numeroVuelo;
	
	}
	public Date darHoraSalida ( ) { 
		return horaSalida;
	}
	
	public Date darHoraLlegada ( ) { 
		return horaLlegada;
	}
	
	public String darTipoEquipo ( ) {
		return tipoEquipo;
	}
	
	public int darNumeroSillas ( ) { 
		return numeroSillas;
	}
	
	public boolean[] darOperacion ( ) {
		return operacion;
	}
	
	public Ciudad darCiudadOrigen ( ) { 
		return origen;
	}
	
	public Ciudad darCiudadDestino ( ) { 
		return destino;
	}
	
	public boolean darTipoVuelo ( ){
		return tipoVuelo;
	}
}
