package mundo;

public class Aerolinea {
	
	private String nombre;
	private double valorMinuto;
	private int tSMax;
	
	public Aerolinea (String nombre, double valorMinuto, int tSMax){
		this.nombre = nombre;
		this.valorMinuto = valorMinuto;
		this.tSMax = tSMax;
	}
	
	public String darNombre () {
		return nombre;
	}
	
	public double darValorMinuto ( ){
		return valorMinuto;
	}
	
	public int darSillasMax ( ){
		return tSMax;
	}
	
}
