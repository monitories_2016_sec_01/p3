package mundo;

import java.util.Date;

import estructuras.GrafoDirigido;

public class AeroCivil {
	
	private GrafoDirigido grafoTodosVuelos;
	private GrafoDirigido grafoAerolinea;
	
	public AeroCivil ( ){
		//TODO
	}
	
	public GrafoDirigido darGrafoTodosVuelos ( ){
		return grafoTodosVuelos;
	}
	
	public GrafoDirigido darGrafoAerolinea ( ){
		return grafoAerolinea;
	}
	
	public void crearCatalogoDeVuelos ( ){
		//TODO
	}
	
	public void agregarAerolinea (String nuevaAerolinea){
		//TODO
	}
	
	public void eliminarAerolinea (String aerolinea){
		
	}
	
	public void agregarVuelo (String aerolinea, int numeroVuelo, Date horaSalida, Date horaLlegada, String tipoEquipo, int numeroSillas, boolean[] operacion, Ciudad origen, Ciudad destino, boolean tipoVuelo){
		
	}
	
	public void conjuntoDeCiudadesConectadasSinAerolineas ( ){
		
	}
	
	public void conjuntoDeCiudadesConectadasConAerolineas ( ){
		
	}
	
	public void MSTVuelosNacionalesConTiempoConAerolineas (String nombreCiudad){
		
	}
	
	public void MSTVuelosNacionalesConCostoConAerolineas (String nombreCiudad, String nombreAerolinea){
		
	}
	
	public void MSTVuelosTodosConTiempoSinAerolineas (String nombreCiudad, Date diaPartida){
		
	}
	
	public void itinerarioDeCostoMinimoParaCadaAerolinea (String ciudadOrigen, String ciudadDestino, Date diaPartida){
		
	}
	
	public void itinerarioCostoMinimoDiferentesAerolineas (String ciudadOrigen, String ciudadDestino, Date diaPartida){
		
	}
	
	public void rutaConCostoMinimoPorCadaAerolinea (String nombreCiudad){
		
	}
	
	public void rutaConTiempoMinimoPorCadaAerolinea (String nombreCiudad){
		
	}
	
	public void crearGrafo (Ciudad ciudad, int peso){
		
	}

}
