package importar;

import java.io.*;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ImportarDatos {
	
	private final static String RUTA = "./data/ItinerarioAeroCivil-v2.xlsx";
	
	public ImportarDatos ( ){
		leerExcel();
	}
	
	@SuppressWarnings({ "resource", "deprecation" })
	public void leerExcel ( ){
		
		try {
			FileInputStream input = new FileInputStream(new File(RUTA));
			XSSFWorkbook libro = new XSSFWorkbook(input);
			XSSFSheet hoja = libro.getSheetAt(0);
			
			Iterator<Row> iteradorFilas = hoja.iterator();
			
			while (iteradorFilas.hasNext()){
				
				Row fila = iteradorFilas.next();
				
				//Iterar sobre las columnas
				Iterator<Cell> iteradorColumna = fila.cellIterator();
				
				while(iteradorColumna.hasNext()){
					
					Cell celda = iteradorColumna.next();
					
					switch (celda.getCellType()){
						case Cell.CELL_TYPE_NUMERIC:
							System.out.println(celda.getNumericCellValue() + "t");
							break;
						case Cell.CELL_TYPE_STRING:
							System.out.println(celda.getStringCellValue() + "t");
							break;
					}
				}
				System.out.println("");
			}
			input.close();
		} catch (IOException e) {
			e.getMessage();
		}
		
	}
	
	public static void main(String[] args){
		new ImportarDatos();
	}
}
