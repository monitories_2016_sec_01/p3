/**
 * El API e implementación de esta clase fue sacado y basado del siguiente libro
 * Goodrich M..Data Structures and Algorithms.6ta Edición. Página 126
 */

package estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Clase gen�rica que modela una lista encadenada
 */
public class ListaEncadenada <T> implements Iterable<T>{
	/**
	 * Clase gen�rica que modela un Nodo de la lista encadenada
	 */	
	public static class Node<T> {
		//-------------------------------------------------------------------------------------------------
		//Atributos
		//-------------------------------------------------------------------------------------------------
		/**
		 * Atributo que modela el objeto que se guarda en la lista
		 */
		private T item;
		/**
		 * Atributo que modela el siguiente objeto que se est� en la lista
		 */
		private Node<T> siguiente;

		//-------------------------------------------------------------------------------------------------
		//Constructor
		//-------------------------------------------------------------------------------------------------
		/**
		 * Crea un nuevo nodo
		 * @param nuevo
		 */
		public Node (T nuevo){
			item = nuevo;
			siguiente = null;
		}
		//-------------------------------------------------------------------------------------------------
		//M�todos
		//-------------------------------------------------------------------------------------------------
		/**
		 * Retorna el objeto que est� almacenado en la lista
		 * @return El objeto T actual
		 */
		public T getItem () {
			return item;
		}
		/**
		 * Retorna el siguiente nodo que est� almacenado en la lista
		 * @return el siguiente nodo, null si es el final de la lista
		 */
		public Node<T> getNext() {
			return siguiente;
		}
		/**
		 * Ingresa el nodo que entra por parametro despu�s del acutal nodo
		 * @param pSig nodo que 
		 */
		public void setNext(Node<T> pSig){
			siguiente = pSig;
		}
	}

	//-------------------------------------------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------------------------------------------
	/**
	 * Primer y �ltimo nodo de la lista
	 */
	private Node<T> primero, ultimo;
	/**
	 * Tama�o de la lista
	 */
	private int n;

	
	//-------------------------------------------------------------------------------------------------
	//Constructor
	//-------------------------------------------------------------------------------------------------
	/**
	 * Inicializa una nueva lista encadenada simple
	 */
	public ListaEncadenada () {
		primero = null;
		ultimo = null;
		n = 0;
	}

	//-------------------------------------------------------------------------------------------------
	//M�todos
	//-------------------------------------------------------------------------------------------------
	/**
	 * 
	 */
	public Node<T> darCabeza ( ){
		return primero;
	}
	
	/**
	 * @return True si la lista est� vac�a, falso de lo contrario.
	 */
	public boolean isEmpty() {
		return n == 0;
	}

	/**
	 * @return Tamanio de la lista
	 */
	public int size() {
		return n;
	}

	/**
	 * @return El primer objeto de la lista, null si est� vac�a
	 */
	public T darPrimero() {
		if (isEmpty())		
			return null;
		else 
		{
			return primero.getItem();
		}
	}

	/**
	 * @return El �ltimo objeto de la lista
	 */
	public T darUltimo( ) {
		if (isEmpty())
			return null;
		else {
			return ultimo.getItem();
		}
	}

	/**
	 * A�ade el nuevo objeto de primero en la lista
	 * @param pPrimero Objeto a anadir
	 */
	public void anadirPrimero (T pPrimero) {
		Node<T> viejo = primero;
		primero = new Node<T>(pPrimero);
		primero.setNext(viejo);
		n ++;
	}

	/**
	 * A�ade el nuevo objeto al final de la lista
	 * @param pUltimo Objeto a anadir
	 */
	public void anadirUltimo (T pUltimo) {
		Node<T> viejo = ultimo;
		ultimo = new Node<T>(pUltimo);
		ultimo.setNext(null);
		if (isEmpty()){
			primero = ultimo;
		} else {
			viejo.setNext(ultimo);
		}
		n ++;
	}
	
	/**
	 * Quita el primer objeto que hay en la lista
	 * @return El objeto que se quita de la lista
	 */
	public T quitarPrimero() {
		T p = null;
		if (!isEmpty()){
			p = primero.getItem();
			primero = primero.getNext();
			n --;
		}

		if (isEmpty()){
			ultimo = null;
		}
		return p;
	}
	

	@Override
	public Iterator<T> iterator() 
	{
		return new ListIterator <T> (primero);
	}
	
	@SuppressWarnings("hiding")
	private class ListIterator <T> implements Iterator<T> {
		private Node<T> actual;
		
		public ListIterator(Node<T> primero){
			actual = primero;
		}
		
		public boolean hasNext( ){
			return actual != null;
		}
		
		public T next ( ){
			if (!hasNext()){
				throw new NoSuchElementException ( );
			}
			T elemento = actual.getItem();
			actual = actual.getNext();
			return elemento;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
}
